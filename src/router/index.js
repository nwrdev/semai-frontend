import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login'
import Home from '../views/Home.vue'
import Pesanan from '../views/Pesanan.vue'
import Keranjang from '../views/Keranjang.vue'
import ProductCategory from '../views/ProductCategory'
import ProductAll from '@/views/ProductAll'
import Search from '@/views/Search'
import ProductDetail from '../views/ProductDetail.vue'
import PetaniProfile from '@/views/PetaniProfile'
import layout from '../layout/layout.vue'
import Secure from '../components/secure.vue'
import ProfilePembeli from '@/views/ProfilePembeli'
import store from '../store/index'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/pesanan',
    name: 'Pesanan',
    meta: {
      value: 'pesanan',
      requiresAuth: true
    },
    component: Pesanan
  },
  {
    path: '/keranjang',
    name: 'Keranjang',
    meta: {
      value: 'keranjang',
      requiresAuth: true
    },
    component: Keranjang
  },
  {
    path: '/product/detail',
    name: 'ProductDetail',
    meta: { value: 'product detail' },
    component: layout,
    children: [
      {
        path: ':name/:id',//buat nerima nama produk sama id produk
        name: 'Product detail',
        meta: {
          value: '',
          requiresAuth: true
        },
        component: ProductDetail
      },
    ]
  },
  {
    path: '/product/',
    name: '',
    meta: { value: 'product' },
    component: layout,
    children: [
      {
        path: '/',
        name: 'All Product',
        meta: { value: '' },
        component: ProductAll
      },
      {
        path: ':kategori',
        name: 'Product Category',
        meta: { value: '' },
        component: ProductCategory
      },
    ]
  },
  {
    path: '/search/',
    name: '',
    meta: { value: 'search' },
    component: layout,
    children: [
      {
        path: '/',
        name: 'All Result',
        meta: { value: '' },
        component: ProductAll
      },
      {
        path: ':searchValue',
        name: 'Search',
        meta: { value: '' },
        component: Search
      },
    ]
  },
  {
    path: '/profile/:id',
    name: 'PetaniProfile',
    meta: {
      value: 'profile',
      requiresAuth: true
    },
    component: PetaniProfile
  },
  {
    path: '/secure',
    name: 'secure',
    component: Secure,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/akun/:id',
    name: 'ProfilePembeli',
    meta: {
      value: 'profile',
      requiresAuth: true
    },
    component: ProfilePembeli
  },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

export default router
