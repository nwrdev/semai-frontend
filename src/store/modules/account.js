import axios from 'axios'

const state = {
    account: {},
    accountUpdate: {},
};

const getters = {
    detailAccount: state => state.account,
};

const actions = {
    // mengambil detail akun
    async fetchAccountDetail({ commit }, _id) {
        await axios.get(`https://semai.herokuapp.com/api/user/${_id}`)
            .then(res => {
                commit('setAccountDetail', res.data);
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
            })
    },
    // mengedit akun
    editProfileUser({ commit }, user) {
        commit('update_account', user);
        console.log('test', user)
        let id = localStorage._id
        return new Promise((resolve, reject) => {
            axios({ url: `https://semai.herokuapp.com/api/user/${id}`, data: user, method: 'PUT' })
                .then(resp => {
                    resolve(resp)
                })
                .catch(err => {
                    commit('auth_error', err)
                    reject(err)
                })
        })
    },
};

const mutations = {
    setAccountDetail: (state, account) => (
        state.account = account
    ),
    update_account: (state, accountUpdate) => {
        state.accountUpdate = accountUpdate
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}
