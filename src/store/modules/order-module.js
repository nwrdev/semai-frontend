import axios from 'axios'

const state = {
    order: [],
    deliveryOrder: [],
    paidOrder: [],
    deliveryDone: []
};

const getters = {
    allOrder: state => state.order,
    allInDelivery: state => state.deliveryOrder,
    allPaidOrder: state => state.paidOrder,
};

const actions = {
    // isi cart yang sudah di kirim ke halaman pesanan
    async getOrderedCart({ commit }) {
        let user = localStorage._id;
        await axios.get(`https://semai.herokuapp.com/api/cart/user/${user}`)
            .then(res => {
                commit('Set_Ordered', res.data);
                let productOrdered = state.order.filter(item => {
                    return item.payment.isPaid === true && item.delivery.isAccepted === false && item.delivery.isDelivery === false && item.delivery.isDone === false//changed
                })
                console.log("order", productOrdered)
                if (!productOrdered) {
                    commit('Set_Ordered',)
                } else {
                    commit('Set_Ordered', productOrdered)
                }
            }).catch(err => {
                console.log('error', err);
            })
    },

    // yang sedang dikirim
    async getInDelivery({ commit }) {
        let user = localStorage._id;
        await axios.get(`https://semai.herokuapp.com/api/cart/user/${user}`)
            .then(res => {
                commit('Set_InDelivery', res.data);
                let productDelivered = state.deliveryOrder.filter(item => {
                    return item.delivery.isDelivery === true && item.delivery.isAccepted === true && item.delivery.isDone === false//changed
                })
                console.log("deliver", productDelivered)
                if (!productDelivered) {
                    commit('Set_InDelivery',)
                } else {
                    commit('Set_InDelivery', productDelivered)
                }
            }).catch(err => {
                console.log('error', err);
            })
    },

    // mengambil list pesanan yang sudah dibayar
    async getPaidCart({ commit }) {
        let user = localStorage._id;
        await axios.get(`https://semai.herokuapp.com/api/cart/user/${user}`)
            .then(res => {
                commit('Set_Paid', res.data);
                let productPaid = state.paidOrder.filter(item => {
                    return item.delivery.isDone === true && item.delivery.isAccepted === true && item.delivery.isDelivery === true//changed
                })
                console.log("pay", productPaid)
                if (!productPaid) {
                    commit('Set_Paid',)
                } else {
                    commit('Set_Paid', productPaid)
                }
            }).catch(err => {
                console.log('error', err);
            })
    },

    // mengirim isi cart ke halaman pesanan
    async addToDone({ commit }, { item }) {
        commit('Set_Done', item);
        return axios.put(`https://semai.herokuapp.com/api/cart/${item._id}`, {
            product: item.product._id,
            user: localStorage._id,
            delivery: {
                isAccepted: true,
                isDelivery: true,
                isDone: true //changed
            },
        })
            .then(res => {
                location.reload();
                console.log(res);
            }).catch(err => {
                console.log('error', err);
            })
    },
};

const mutations = {
    Set_Ordered: (state, allOrder) => {
        state.order = allOrder;
    },

    Set_InDelivery: (state, allDelivery) => {
        state.deliveryOrder = allDelivery
    },

    Set_Paid: (state, allPaidOrder) => {
        state.paidOrder = allPaidOrder;
    },

    Set_Done: (state, item) => {
        state.deliveryDone = item;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}
