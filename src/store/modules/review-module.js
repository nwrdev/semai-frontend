import axios from 'axios'

// f untuk farmer
// p untuk produk

const state = {
    reviewsbyfid: [],
    reviewsbypid: [],
};

const getters = {
    allReviewsbyfid: state => state.reviewsbyfid,
    allReviewsbypid: state => state.reviewsbypid,
};

const actions = {
    // memanggil ulasan berdasarkan farmer id
    async fetchReviewsbyfid({ commit }, id) {
        commit('setLoading', true)
        await axios.get(`https://semai.herokuapp.com/api/review/farmer/${id}`)
            .then(res => {
                commit('setReviewsbyfid', res.data);
                commit('setLoading', false);
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },
};

const mutations = {
    setReviewsbyfid: (state, reviewsbyfid) => {
        state.reviewsbyfid = reviewsbyfid
    },
};

export default {
    state,
    getters,
    actions,
    mutations
}
