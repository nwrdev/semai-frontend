import axios from 'axios'

const state = {

};

const getters = {

};

const actions = {
    // login dengan autentikasi token, juga menyimpan token dan id ke localStorage
    login({ commit }, user) {
        return new Promise((resolve, reject) => {
            commit('setLoading', true)
            commit('auth_request')
            axios({ url: 'https://semai.herokuapp.com/api/user/login', data: user, method: 'POST' })
                .then(resp => {
                    const token = resp.data.token
                    const user = resp.data.user
                    const _id = resp.data._id
                    localStorage.setItem('token', token)
                    localStorage.setItem('_id', _id)
                    axios.defaults.headers.common['Authorization'] = token
                    commit('auth_success', token, user)
                    commit('setLoading', false);
                    resolve(resp)
                })
                .catch(err => {
                    commit('auth_error')
                    localStorage.removeItem('token')
                    commit('setLoading', false);
                    reject(err)
                })
        })
    },

    // register untuk mendapat token dan id
    register({ commit }, user) {
        return new Promise((resolve, reject) => {
            commit('setLoading', true)
            commit('auth_request')
            axios({ url: 'https://semai.herokuapp.com/api/user', data: user, method: 'POST' })
                .then(resp => {
                    const token = resp.data.token
                    const user = resp.data.user
                    localStorage.setItem('token', token)
                    axios.defaults.headers.common['Authorization'] = token
                    commit('auth_success', token, user)
                    alert("Berhasil, silahkan masukkan email dan password untuk login")
                    commit('setLoading', false);
                    resolve(resp)
                })
                .catch(err => {
                    commit('auth_error', err)
                    localStorage.removeItem('token')
                    alert("Akun Sudah Terdaftar")
                    commit('setLoading', false);
                    reject(err)
                })
        })
    },

    // menghapus token dan id dari localStorage, juga menghapus otorisasi untuk masuk ke halaman yang butuh auth
    logout({ commit }) {
        return new Promise((resolve) => {
            commit('logout')
            localStorage.removeItem('token')
            localStorage.removeItem('_id')
            delete axios.defaults.headers.common['Authorization']
            resolve()
        })
    }
};

const mutations = {

};

export default {
    state,
    getters,
    actions,
    mutations
}
